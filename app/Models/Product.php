<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'article',
        'status',
        'sku',
        'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 'available');
    }
}
