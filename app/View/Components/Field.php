<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Field extends Component
{
    /**
     * The field display type.
     *
     * @var string
     */
    public $type;

    /**
     * The field display label.
     *
     * @var string
     */
    public $label;

    /**
     * The field display items.
     *
     * @var array
     */
    public $items;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $type, string $label, array $items)
    {
        $this->type = $type;
        $this->label = $label;
        $this->items = $items;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.field');
    }
}
