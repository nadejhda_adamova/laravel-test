<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;


Route::group(['prefix' => 'dashboard'], function() {
    Route::get('product/create', [ProductController::class, 'create'])
        ->name('product.create');
    Route::post('product/create', [ProductController::class, 'store'])
        ->name('product.store');

    Route::get('product/{product}', [ProductController::class, 'show'])
        ->name('product.show');

    Route::get('product/{product}/edit', [ProductController::class, 'edit'])
        ->name('product.edit');
    Route::post('product/{product}/update', [ProductController::class, 'update'])
        ->name('product.update');

    Route::get('product/{product}/delete', [ProductController::class, 'delete'])
        ->name('product.delete');
    Route::post('product/{product}/delete', [ProductController::class, 'destroy'])
        ->name('product.destroy');

    Route::get('product', [ProductController::class, 'all'])
            ->name('product.all');
});
