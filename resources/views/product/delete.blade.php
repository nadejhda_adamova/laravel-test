<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="font-semibold text-xl text-gray-800 leading-tight my-4">Delete product "{{  $product->name }}"</h3>

                    <form method="POST" action="{{ route('product.destroy', ['product' => $product]) }}">
                        @csrf
                        <x-input id="id" class="block mt-1 w-full"
                                        type="hidden"
                                        name="id"
                                        value="{{  $product->id }}"/>
                        <div class="mt-4">
                          {{ __('Do you want to delete product with id "') }}<strong>{{ $product->id }}</strong>" ?
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-4">
                                {{ __('Delete product') }}
                            </x-button>
                            <x-button class="ml-4">
                                {{ __('Cancel') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
