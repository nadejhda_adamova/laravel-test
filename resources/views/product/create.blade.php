<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create product') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                  <form method="POST" action="{{ route('product.store') }}">
                      @csrf
                      <div class="mt-4">
                          <x-label for="name" :value="__('Name')" />
                          <x-input id="name" class="block mt-1 w-full"
                                          type="text"
                                          name="name"
                                          required/>
                      </div>
                      <div class="mt-4">
                          <x-label for="sku" :value="__('Article')" />
                          <x-input id="sku" class="block mt-1 w-full"
                                          type="text"
                                          name="sku"
                                          required/>
                      </div>
                      <div class="mt-4">
                          <x-label for="status" :value="__('Status')" />
                          <x-select id="status" class="block mt-1 w-full"
                                          type="select"
                                          name="status"
                                           >
                                           <option value="available">available</option>
                                           <option value="unavailable">unavailable</option>
                                         </x-select>
                      </div>
                      <div class="mt-4">
                        <x-label for="data[color]" :value="__('Color')" />
                        <x-input class="block mt-1 w-full"
                                        type="text"
                                        name="data[color]"
                                        required/>
                      </div>
                      <div class="mt-4">
                        <x-label for="data[size]" :value="__('Size')" />
                        <x-input class="block mt-1 w-full"
                                        type="text"
                                        name="data[size]"
                                        required/>
                      </div>
                      <div class="flex items-center justify-end mt-4">
                          <x-button class="ml-4">
                              {{ __('Save product') }}
                          </x-button>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
