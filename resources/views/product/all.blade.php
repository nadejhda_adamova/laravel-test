<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Available products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-6">
          <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200 flex justify-between">
              <form method="GET">
                <label for="available" class="inline-flex items-center">
                    <input id="available" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="available" {{ app('request')->input('available') ? 'checked' : '' }}>
                    <span class="ml-2 text-sm text-gray-600">{{ __('Only available') }}</span>
                </label>
                <x-button class="ml-3">
                    {{ __('Submit') }}
                </x-button>
              </form>
              s
            </div>
          </div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                  <table class="w-full">
                    <thead>
                      <tr class="border-b">
                        <th>#</th>
                        <th class="text-left py-2">{{ __('Name')}}</th>
                        <th class="text-left py-2">{{ __('Article')}}</th>
                        <th class="text-left py-2">{{ __('Status')}}</th>
                        <th class="text-left py-2">{{ __('Color')}}</th>
                        <th class="text-left py-2">{{ __('Size')}}</th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach ($products as $product)
                      <tr>
                        <td class="text-center py-3">
                          {{ $product->id }}
                        </td>
                        <td class="py-3">
                          {{ $product->name }}
                        </td>
                        <td class="py-3">
                          {{ $product->sku }}
                        </td>
                        <td class="py-3">
                          {{ $product->status }}
                        </td>
                        <td class="py-3">
                          {{ $product->data['color'] }}
                        </td>
                        <td class="py-3">
                          {{ $product->data['size'] }}
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
