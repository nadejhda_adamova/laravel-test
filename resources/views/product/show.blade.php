<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __($product->name) }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                  <x-field :type="$type ?? 'inline'" :label="$label ?? 'Article'" :items="[$product->sku]"/>
                  <x-field :type="$type ?? 'inline'" :label="$label ?? 'Status'" :items="[$product->status]"/>
                  @if(null !== $product->data)
                    @foreach ($product->data as $label => $item)
                    <x-field :type="$type ?? 'inline'" :label="$label ?? 'Data'" :items="[$item]"/>
                      @endforeach
                  @endif
                </div>

                <div class="flex justify-end px-6 py-4">
                  <a href="{{ route('product.edit', ['product' => $product]) }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring ring-gray-300 disabled:opacity-25 transition ease-in-out duration-150 ml-4">
                    {{ __('Edit product') }}
                  </a>
                  <a href="{{ route('product.delete', ['product' => $product]) }}" class="inline-flex items-center px-4 py-2 bg-red-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-800 active:bg-red-800 focus:outline-none focus:border-red-800 focus:ring disabled:opacity-25 transition ease-in-out duration-150 ml-4">
                    {{ __('Delete product') }}
                  </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
