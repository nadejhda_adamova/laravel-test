@php
switch ($type) {
    case 'inline':
        $fieldClasses = 'flex mt-4';
        break;
    case 'above':
        $fieldClasses = 'flex flex-col mt-4';
        break;
    default:
        $fieldClasses = 'flex mt-4';
        break;
}
@endphp
<div class="field field--label-display-{{ $type }} {{ $fieldClasses }}">
    @if ($label)
        <div class="field__label mr-2 font-semibold">{{ $label ?? 'inline' }}</div>
    @endif
    <div class="field__items">
      @foreach ($items as $item)
          <div class="field__item">{{ $item }}</div>
      @endforeach
    </div>
</div>
