<?php

return [
    'products' => [
        'role' => [
            // ROLE: ADMIN
            'admin' => [
                'admin.product.create',
                'admin.product.read',
                'admin.product.update',
                'admin.product.delete',
                'contributor.product.update_sku',
            ],
            // ROLE: USER
            'contributor' => [
                'contributor.product.create',
                'contributor.product.read',
                'contributor.product.update',
            ],
        ],

    ],

];
